(* ========================================================================= *)
(* Quantum(?) vectors.                                                       *)
(* (Complex) vectors indexed by array of bits (of fixed length).             *)
(*                                                                           *)
(* (c) Copyright, Andrea Gabrielli, Marco Maggesi 2017-2018                  *)
(* ========================================================================= *)

(* ------------------------------------------------------------------------- *)
(* Miscellanea.                                                              *)
(* ------------------------------------------------------------------------- *)

let VSUM_EQ_COMPLEX_0 = prove
 (`!f s. (!x:A. x IN s ==> f x = Cx(&0)) ==> vsum s f = Cx(&0)`,
  REWRITE_TAC[GSYM COMPLEX_VEC_0; VSUM_EQ_0]);;

(* ------------------------------------------------------------------------- *)
(* Bitvectors.                                                               *)
(*                                                                           *)
(* Bijection between boolean vectors and set of naturals:                    *)
(*   bvecset : bool^N -> (num->bool)                                         *)
(*   setbvec : (num->bool) -> bool^N                                         *)
(*                                                                           *)
(* Identities:                                                               *)
(*   BVECSET_SETBVEC                                                         *)
(*     |- !s:bool^N. bvecset (setbvec s) = s INTER (1..dimindex(:N))         *)
(*   SETBVEC_BVECSET                                                         *)
(*     |- !bs:bool^N. setbvec (bvecset bs) = bs                              *)
(* ------------------------------------------------------------------------- *)

let bvecset = new_definition
  `bvecset(bs:bool^N) = {i | 1 <= i /\ i <= dimindex(:N) /\ bs$i}`;;

let IN_BVECSET = prove
 (`!i bs:bool^N. i IN bvecset bs <=> 1 <= i /\ i <= dimindex(:N) /\ bs$i`,
  REWRITE_TAC[bvecset; IN_ELIM_THM]);;

let BVECSET_INJ = prove
 (`!bs bs':bool^N. bvecset bs = bvecset bs' <=> bs = bs'`,
  REWRITE_TAC[CART_EQ; EXTENSION; IN_BVECSET] THEN MESON_TAC[]);;

let setbvec = new_definition
  `setbvec s:bool^N = lambda i. i IN s`;;

let SETBVEC_COMPONENT = prove
 (`!s i. 1 <= i /\ i <= dimindex(:N) ==> ((setbvec s:bool^N)$i <=> i IN s)`,
  SIMP_TAC[setbvec; LAMBDA_BETA]);;

let BVECSET_SETBVEC = prove
 (`!s. bvecset (setbvec s:bool^N) = s INTER (1..dimindex(:N))`,
  GEN_TAC THEN REWRITE_TAC[EXTENSION] THEN
  INTRO_TAC "![i]" THEN REWRITE_TAC[IN_INSERT] THEN
  REWRITE_TAC[IN_BVECSET; IN_INTER; IN_NUMSEG] THEN
  ASM_CASES_TAC `1 <= i` THEN ASM_REWRITE_TAC[] THEN
  ASM_CASES_TAC `i <= dimindex(:N)` THEN ASM_REWRITE_TAC[] THEN
  ASM_SIMP_TAC[SETBVEC_COMPONENT]);;

let SETBVEC_BVECSET = prove
 (`!bs:bool^N. setbvec (bvecset bs) = bs`,
  SIMP_TAC[CART_EQ; IN_BVECSET; SETBVEC_COMPONENT]);;

let BVECSET_BOUNDS = prove
 (`!bs:bool^N. bvecset bs SUBSET 1..dimindex(:N)`,
  REWRITE_TAC[bvecset; SUBSET; FORALL_IN_GSPEC; IN_NUMSEG] THEN
  REPEAT STRIP_TAC THEN ASM_REWRITE_TAC[]);;

(* ------------------------------------------------------------------------- *)
(* bitscode & codebits.                                                      *)
(*                                                                           *)
(* Bijection between boolean vectors and natual numbers.                     *)
(*   bitscode : bool^N -> num                                                *)
(*   codebits : num -> bool^N                                                *)
(*                                                                           *)
(* Identities:                                                               *)
(*   BITSCODE_CODEBITS                                                       *)
(*     |- !n. 1 <= n /\ n <= 2 EXP dimindex(:N)                              *)
(*            ==> bitscode(codebits n:bool^N) = n                            *)
(*   CODEBITS_BITSCODE                                                       *)
(*     |- !bs:bool^N. codebits (bitscode bs) = bs                            *)
(* ------------------------------------------------------------------------- *)

let bitscode = new_definition
  `bitscode (bs:bool^N) = setcode (bvecset bs)`;;

let codebits = new_definition
  `codebits n:bool^N = setbvec (codeset n)`;;

let BITSCODE_BOUNDS = prove
 (`!bs:bool^N. 1 <= bitscode bs /\ bitscode bs <= 2 EXP dimindex(:N)`,
  GEN_TAC THEN REWRITE_TAC[bitscode; GSYM IN_NUMSEG] THEN
  MATCH_MP_TAC SETCODE_BOUNDS THEN MATCH_ACCEPT_TAC BVECSET_BOUNDS);;

let BITSCODE_CODEBITS = prove
 (`!n. 1 <= n /\ n <= 2 EXP dimindex(:N) ==> bitscode(codebits n:bool^N) = n`,
  REWRITE_TAC[bitscode; codebits; BVECSET_SETBVEC; GSYM IN_NUMSEG] THEN
  MESON_TAC[SET_RULE `s:num->bool SUBSET t ==> s INTER t = s`;
            CODESET_SETCODE_BIJECTIONS]);;

let CODEBITS_BITSCODE = prove
 (`!bs:bool^N. codebits(bitscode bs) = bs`,
  REWRITE_TAC[codebits; bitscode] THEN
  MESON_TAC[CODESET_SETCODE_BIJECTIONS; SETBVEC_BVECSET; BVECSET_BOUNDS]);;

let BITSCODE_INJ = prove
 (`!bs1 bs2:bool^N. bitscode bs1 = bitscode bs2 <=> bs1 = bs2`,
  MESON_TAC[CODEBITS_BITSCODE]);;

let CODEBITS_SURJ = prove
 (`!bs:bool^N. ?n. bs = codebits n`,
  MESON_TAC[CODEBITS_BITSCODE]);;

(* ------------------------------------------------------------------------- *)
(* qvectors.                                                                 *)
(* Vectors indexed by boolean vectors.                                       *)
(* ------------------------------------------------------------------------- *)

make_overloadable "$$" `:A^(N)multivector->B->A`;;

overload_interface("$$",`($$):real^(N)multivector->(num->bool)->real`);;
overload_interface("$$",`qindex:A^(N)multivector->bool^N->A`);;

let qindex = new_definition
  `(x:A^(N)multivector)$$bs = x$bitscode bs`;;

parse_as_binder "qlambda";;

let qlambda = new_definition
 `(qlambda) (g:bool^N->A) =
    (lambda i. g(codebits i)):A^(N)multivector`;;

let QVECTOR_EQ = prove
 (`!u v:A^N multivector. u = v <=> !bs. u$$bs = v$$bs`,
  REPEAT GEN_TAC THEN REWRITE_TAC[CART_EQ; qindex; DIMINDEX_MULTIVECTOR] THEN
  EQ_TAC THEN DISCH_TAC THEN REPEAT STRIP_TAC THENL
  [FIRST_X_ASSUM MATCH_MP_TAC THEN
   REWRITE_TAC[BITSCODE_BOUNDS];
   SUBGOAL_THEN `i = bitscode(codebits i:bool^N)` SUBST1_TAC THENL
   [ASM_SIMP_TAC[BITSCODE_CODEBITS]; ASM_REWRITE_TAC[]]]);;

(* ------------------------------------------------------------------------- *)
(* Crucial properties.                                                       *)
(* ------------------------------------------------------------------------- *)

let QLAMBDA_BETA = prove
 (`!f:bool^N->A bs:bool^N. (qlambda) f $$ bs = f bs`,
  REPEAT GEN_TAC THEN REWRITE_TAC[qlambda; qindex] THEN
  MESON_TAC[LAMBDA_BETA; CODEBITS_BITSCODE; BITSCODE_BOUNDS;
            DIMINDEX_MULTIVECTOR]);;

let QVECTOR_UNIQUE = prove
 (`!v:A^(N)multivector g. (!bs:bool^N. v$$bs = g bs) ==> (qlambda) g = v`,
  REWRITE_TAC[QVECTOR_EQ] THEN SIMP_TAC[QLAMBDA_BETA]);;

let QVECTOR_ETA = prove
 (`(qlambda bs. v$$bs) = v:A^(N)multivector`,
  SIMP_TAC[QVECTOR_EQ; QLAMBDA_BETA]);;

(* ------------------------------------------------------------------------- *)
(* Also componentwise operations; they all work in this style.               *)
(* ------------------------------------------------------------------------- *)

let QVECTOR_ADD_COMPONENT = prove
 (`!x y:complex^(N)multivector bs:bool^N. (x + y)$$bs = x$$bs + y$$bs`,
  REWRITE_TAC[qindex; MATRIX_ADD_ROW]);;

let QVECTOR_SUB_COMPONENT = prove
 (`!x y:complex^(N)multivector bs. (x - y)$$bs = x$$bs - y$$bs`,
  REWRITE_TAC[qindex; MATRIX_SUB_ROW]);;

let QVECTOR_NEG_COMPONENT = prove
 (`!x:complex^(N)multivector bs. (--x)$$bs = --(x$$bs)`,
  REWRITE_TAC[qindex; MATRIX_NEG_ROW]);;

let QVECTOR_MUL_COMPONENT = prove
 (`!c x:complex^(N)multivector bs. (c % x)$$bs = c * x$$bs`,
  REWRITE_TAC[qindex; CVECTOR_MUL_COMPONENT]);;

let QVECTOR_CONST_COMPONENT = prove
 (`!k bs. (vector_const k:complex^(N)multivector)$$bs = k`,
  REWRITE_TAC[qindex; VECTOR_CONST_COMPONENT]);;

let QVECTOR_MSUM_COMPONENT = prove
 (`!f:A->complex^(N)multivector t bs. msum t f$$bs = vsum t (\x. f x$$bs)`,
  REWRITE_TAC[qindex; MSUM_ROW]);;

let QVECTOR_MSUM = prove
 (`!t f. msum t f =
         qlambda bs. vsum t (\x:A. (f x:complex^(N)multivector)$$bs)`,
  SIMP_TAC[QVECTOR_EQ; QLAMBDA_BETA; QVECTOR_MSUM_COMPONENT]);;

let VECTOR_MAP_QINDEX = prove
 (`!f:A->B v bs:bool^N. vector_map f v $$ bs = f (v $$ bs)`,
  REWRITE_TAC[vector_map; qindex] THEN
  SIMP_TAC[LAMBDA_BETA; BITSCODE_BOUNDS; DIMINDEX_MULTIVECTOR]);;

let VECTOR_TO_CVECTOR_QINDEX = prove
 (`!v bs:bool^N. vector_to_cvector v $$ bs = Cx(v $$ bs)`,
  REWRITE_TAC[vector_to_cvector; VECTOR_MAP_QINDEX]);;

let QVECTOR_MAT_0_COMPONENT = prove
 (`!bs. mat 0:complex^N multivector $$ bs = Cx(&0)`,
  REWRITE_TAC[qindex; MAT_0_ROW; COMPLEX_VEC_0]);;

(* ------------------------------------------------------------------------- *)
(* Basis vectors indexed by bitstrings.                                      *)
(* ------------------------------------------------------------------------- *)

let qbasis = new_definition
  `qbasis (bs:bool^N):complex^(N)multivector =
   vector_to_cvector (mbasis (bvecset bs))`;;

let QBASIS_COMPONENT = prove
 (`!bs bs':bool^N. qbasis bs$$bs' = if bs = bs' then Cx(&1) else Cx(&0)`,
  REWRITE_TAC[qbasis; VECTOR_TO_CVECTOR_QINDEX] THEN
  REWRITE_TAC[qindex; bitscode; GSYM sindex] THEN
  METIS_TAC[MBASIS_COMPONENT; BVECSET_INJ; BVECSET_BOUNDS]);;

let QBASIS_EXPANSION = prove
 (`!v. msum (:bool^N) (\bs. v$$bs % qbasis bs) = v`,
  REWRITE_TAC[QVECTOR_EQ; QVECTOR_MSUM_COMPONENT; QVECTOR_MUL_COMPONENT;
    QBASIS_COMPONENT; COND_RAND; COMPLEX_MUL_RZERO; COMPLEX_MUL_RID] THEN
  REPEAT GEN_TAC THEN
  SUBGOAL_THEN `!v x bs:bool^N. (if x = bs then v$$x else Cx(&0)) =
                                (if x = bs then v$$bs else Cx(&0))`
    (fun th -> REWRITE_TAC[th; COMPLEX_VSUM_DELTA; IN_UNIV]) THEN
  REPEAT GEN_TAC THEN COND_CASES_TAC THEN ASM_REWRITE_TAC[]);;

let FORALL_QBASIS = prove
 (`(!v:complex^(N)multivector. P v) <=>
   (!w. P (msum (:bool^N) (\bs. w bs % qbasis bs)))`,
  MESON_TAC[QBASIS_EXPANSION]);;

let QBASIS_NONZERO = prove
 (`!bs. ~(qbasis bs:complex^(N)multivector = mat 0)`,
  GEN_TAC THEN REWRITE_TAC[QVECTOR_EQ; QBASIS_COMPONENT; MAT_0_COMPONENT] THEN
  REWRITE_TAC[NOT_FORALL_THM; qindex; MAT_0_ROW; COMPLEX_VEC_0] THEN
  EXISTS_TAC `bs:bool^N` THEN REWRITE_TAC[] THEN CONV_TAC COMPLEX_RING);;

let QBASIS_INJ = prove
 (`!bs1 bs2:bool^N. qbasis bs1 = qbasis bs2 ==> bs1 = bs2`,
  REPEAT GEN_TAC THEN
  DISCH_THEN (MP_TAC o AP_TERM `\v:complex^N multivector. v $$ bs1`) THEN
  REWRITE_TAC[QBASIS_COMPONENT] THEN
  COND_CASES_TAC THEN ASM_REWRITE_TAC[CX_INJ] THEN
  REAL_ARITH_TAC);;

let QBASIS_INJ_EQ = prove
 (`!bs1 bs2:bool^N. qbasis bs1 = qbasis bs2 <=> bs1 = bs2`,
  MESON_TAC[QBASIS_INJ]);;

let CINDEPENDENT_QBASIS = prove
 (`cindependent {qbasis bs | bs IN (:bool^N)}`,
  CLAIM_TAC "finbvec" `FINITE (:bool^N)` THENL
  [SIMP_TAC[FINITE_CART_UNIV; FINITE_BOOL]; ALL_TAC] THEN
  SUBST1_TAC
    (SET_RULE `{qbasis bs | bs IN (:bool^N)} = IMAGE qbasis (:bool^N)`) THEN
  CLAIM_TAC "fin" `FINITE (IMAGE qbasis (:bool^N))` THENL
  [ASM_SIMP_TAC[FINITE_IMAGE]; ALL_TAC] THEN
  ASM_REWRITE_TAC[CINDEPENDENT_EXPLICIT] THEN
  GEN_TAC THEN ASM_SIMP_TAC[MSUM_IMAGE; QBASIS_INJ_EQ] THEN
  REWRITE_TAC[o_DEF] THEN
  DISCH_TAC THEN REWRITE_TAC[FORALL_IN_IMAGE; IN_UNIV] THEN
  X_GEN_TAC `bs:bool^N` THEN
  POP_ASSUM (MP_TAC o AP_TERM `\v:complex^N multivector. v $$ bs`) THEN
  REWRITE_TAC[QVECTOR_MSUM_COMPONENT; QVECTOR_MAT_0_COMPONENT;
              QVECTOR_MUL_COMPONENT; QBASIS_COMPONENT] THEN
  SUBGOAL_THEN
    `(\x:bool^N. c (qbasis x) * (if x = bs then Cx(&1) else Cx(&0))) =
     (\x.  if x = bs then c (qbasis bs) else vec 0)`
    (fun th -> REWRITE_TAC[VSUM_DELTA; IN_UNIV; th]) THEN
  REWRITE_TAC[FUN_EQ_THM] THEN GEN_TAC THEN COND_CASES_TAC THEN
  ASM_REWRITE_TAC[COMPLEX_MUL_RID; COMPLEX_MUL_RZERO; COMPLEX_VEC_0]);;

let QBASIS_CBASIS = prove
 (`!bs. qbasis bs:complex^(N)multivector = cbasis (bitscode bs)`,
  GEN_TAC THEN REWRITE_TAC[QVECTOR_EQ; QBASIS_COMPONENT; qindex] THEN
  SIMP_TAC[CBASIS_COMPONENT; BITSCODE_BOUNDS; DIMINDEX_MULTIVECTOR] THEN
  REWRITE_TAC[BITSCODE_INJ] THEN MESON_TAC[]);;

let NORM_QBASIS = prove
 (`!bs. norm(qbasis bs:complex^(N)multivector) = &1`,
  GEN_TAC THEN REWRITE_TAC[QBASIS_CBASIS] THEN
  SIMP_TAC[NORM_CBASIS; BITSCODE_BOUNDS; DIMINDEX_MULTIVECTOR]);;

let QVECTOR_CDOT = prove
 (`!x y:complex^(N)multivector.
        x cdot y = vsum (:bool^N) (\bs:bool^N. x $$ bs * cnj (y $$ bs))`,
  REPEAT GEN_TAC THEN REWRITE_TAC[cdot] THEN
  SUBGOAL_THEN
     `(1..dimindex(:(N)multivector)) = IMAGE (bitscode:bool^N->num) (:bool^N)`
  SUBST1_TAC THENL
  [MATCH_MP_TAC SUBSET_ANTISYM THEN
   REWRITE_TAC[SUBSET; IN_NUMSEG; DIMINDEX_MULTIVECTOR; IN_IMAGE; IN_UNIV] THEN
   MESON_TAC[BITSCODE_BOUNDS; BITSCODE_CODEBITS;
             CODEBITS_BITSCODE;CODEBITS_SURJ]; ALL_TAC] THEN
   MP_TAC (ISPECL [`bitscode:bool^N->num`;
                   `(\i. (x:complex^(N)multivector)$i *
                         cnj (y:complex^(N)multivector$i))`;
                   `(:bool^N)`] VSUM_IMAGE) THEN
   ANTS_TAC THENL
   [SIMP_TAC[IN_UNIV; FINITE_BOOL; FINITE_CART_UNIV] THEN
    MESON_TAC[CODEBITS_BITSCODE]; ALL_TAC] THEN
   SIMP_TAC[] THEN MATCH_MP_TAC (MESON[]`!a b. a ==> (b ==> a)`) THEN
   MATCH_MP_TAC VSUM_EQ THEN
   GEN_TAC THEN REWRITE_TAC[IN_UNIV; o_THM; qindex]);;

let QBASIS_CDOT = prove
 (`!(bs:bool^N) bs'.
    (qbasis bs) cdot (qbasis bs') = if bs = bs' then Cx(&1) else Cx(&0)`,
  REPEAT GEN_TAC THEN REWRITE_TAC[QVECTOR_CDOT] THEN COND_CASES_TAC THENL
  [ASM_SIMP_TAC[QBASIS_COMPONENT] THEN
   SUBGOAL_THEN
      `(\x:bool^N. (if bs' = x then Cx (&1) else Cx (&0)) *
        cnj (if bs' = x then Cx (&1) else Cx (&0))) =
       (\x:bool^N. if x = bs' then Cx(&1) else Cx(&0))` SUBST1_TAC THENL
   [REWRITE_TAC[FUN_EQ_THM] THEN GEN_TAC THEN
    MESON_TAC[COMPLEX_MUL_RZERO; COMPLEX_MUL_LZERO; CNJ_CX;
              COMPLEX_MUL_LID; COMPLEX_MUL_RID]; ALL_TAC] THEN
   REWRITE_TAC[COMPLEX_VSUM_DELTA; IN_UNIV];
   ASM_SIMP_TAC[QBASIS_COMPONENT; GSYM COMPLEX_VEC_0] THEN
   MATCH_MP_TAC VSUM_EQ_0 THEN
   REWRITE_TAC[IN_UNIV] THEN GEN_TAC THEN
   ASM_MESON_TAC[COMPLEX_MUL_RZERO; COMPLEX_MUL_LZERO; CNJ_CX;
                 COMPLEX_VEC_0]]);;

let CSPAN_QBASIS = prove
 (`cspan {qbasis bs | bs IN (:bool^N)} = (:complex^(N)multivector)`,
   SUBGOAL_THEN
     `{qbasis bs | bs IN (:bool^N)} =
      {cbasis s:complex^(N)multivector | 1 <= s /\
                                         s <= dimindex (:(N)multivector)}`
     (fun th -> MESON_TAC[CSPAN_STDCBASIS; th]) THEN
   MATCH_MP_TAC SUBSET_ANTISYM THEN
   REWRITE_TAC[SUBSET; DIMINDEX_MULTIVECTOR; FORALL_IN_GSPEC; IN_UNIV] THEN
   REWRITE_TAC[IN_ELIM_THM; QBASIS_CBASIS] THEN
   MESON_TAC[BITSCODE_CODEBITS; CODEBITS_BITSCODE; BITSCODE_BOUNDS]);;

(* ------------------------------------------------------------------------- *)
(*  Very used conversion to calculate component of a qvector..               *)
(* ------------------------------------------------------------------------- *)

let QVECTOR_COMPONENT_CONV =
   REWRITE_CONV[QVECTOR_ADD_COMPONENT;
                QVECTOR_NEG_COMPONENT; QBASIS_COMPONENT; QVECTOR_SUB_COMPONENT;
                QVECTOR_MUL_COMPONENT; FINVEC_EQ; COMPLEX_MUL_RID;
                COMPLEX_MUL_LID; COMPLEX_MUL_LZERO; COMPLEX_MUL_RZERO;
                COMPLEX_ADD_RID; COMPLEX_ADD_LID; CVECTOR_MUL_LZERO];;

(* ------------------------------------------------------------------------- *)
(* Conversion to rewrites a quantum register v:complex^(N)multivector        *)
(*   as a linear combination of the standard computational basis.            *)
(* ------------------------------------------------------------------------- *)

let QUANTUM_REGISTER_CONV =
 (GEN_REWRITE_CONV I [GSYM QBASIS_EXPANSION] THENC
  GEN_REWRITE_CONV TOP_DEPTH_CONV [BOOL_MSUM_FINVEC] THENC
  DEPTH_CONV BETA_CONV THENC
  GEN_REWRITE_CONV TOP_DEPTH_CONV
   [GSYM MATRIX_ADD_ASSOC; QVECTOR_ADD_COMPONENT; QVECTOR_SUB_COMPONENT;
    QVECTOR_MUL_COMPONENT; QVECTOR_NEG_COMPONENT]);;

(* Tests *)
(*
QUANTUM_REGISTER_CONV `x + y:complex^(3)multivector`;;
QUANTUM_REGISTER_CONV `x - y:complex^(3)multivector`;;
QUANTUM_REGISTER_CONV `x:complex^(7)multivector`;;
QUANTUM_REGISTER_CONV `x:complex^(2)multivector`;;
QUANTUM_REGISTER_CONV `x:complex^(11)multivector`;;
QUANTUM_REGISTER_CONV `x % y:complex^(3)multivector`;;
QUANTUM_REGISTER_CONV `-- y:complex^(3)multivector`;;
*)

(* ------------------------------------------------------------------------- *)
(* Conversion to compute the scalar product of 2 quantum registers.          *)
(* ------------------------------------------------------------------------- *)

let QUANTUM_CDOT_CONV : conv =
 fun tm ->  let y = rand tm and x = rand (rator tm) in
(ONCE_REWRITE_CONV[QUANTUM_REGISTER_CONV y; QUANTUM_REGISTER_CONV x] THENC
 SIMP_CONV[FINITE_BOOL; FINITE_CART_UNIV; CDOT_LSUM; CDOT_RSUM] THENC
 REWRITE_CONV[CDOT_LADD; CDOT_RADD; CDOT_LMUL; CDOT_RMUL;
              CDOT_LSUB; CDOT_RSUB; QBASIS_CDOT; FINVEC_EQ; COMPLEX_MUL_LZERO;
              COMPLEX_MUL_RZERO; COMPLEX_ADD_RID; COMPLEX_ADD_LID;
              COMPLEX_MUL_LID; COMPLEX_MUL_RID; GSYM COMPLEX_NORM_POW_2;
              QBASIS_COMPONENT; CNJ_CX]) tm;;

(* Tests
QUANTUM_CDOT_CONV `(x:complex^(3)multivector) cdot y`;;
QUANTUM_CDOT_CONV `(x:complex^(2)multivector) cdot x`;;
QUANTUM_CDOT_CONV `(a % qbasis <|F;F|> + b % qbasis <|T;F|>) cdot
                   (a % qbasis <|F;F|> + b % qbasis <|T;T|>)`;;
QUANTUM_CDOT_CONV `(x:complex^(N)multivector) cdot x`;;*)

let CNORM_SQRT = prove
 (`!v:complex^(N). norm v = sqrt (norm (v cdot v))`,
  GEN_TAC THEN
  SUBGOAL_THEN `norm (v:complex^N) = sqrt (norm v pow 2)` SUBST1_TAC THENL
  [SIMP_TAC[CNORM_POS; GSYM POW_2_SQRT]; ALL_TAC] THEN
  REWRITE_TAC[CNORM2_ALT]);;

let QUANTUM_SQUARED_NORM = prove
 (`!v:complex^(N)multivector.
     norm v pow 2 = sum (:bool^N) (\bs. norm (v$$bs) pow 2)`,
  GEN_TAC THEN
  REWRITE_TAC[CNORM2] THEN
  SUBGOAL_THEN
    `(v:complex^(N)multivector) cdot v =
     Cx (sum (:bool^N) (\bs. norm (v $$ bs) pow 2))`
  (fun th -> REWRITE_TAC[REAL_OF_COMPLEX_CX; th]) THEN
  CONV_TAC (DEPTH_CONV QUANTUM_CDOT_CONV) THEN
  REWRITE_TAC[GSYM VSUM_CX] THEN
  MATCH_MP_TAC VSUM_EQ THEN
  GEN_TAC THEN REWRITE_TAC[IN_UNIV] THEN
  SUBGOAL_THEN
    `(\x'. (v:complex^(N)multivector) $$ x * cnj (v $$ x') *
      (if x = x' then Cx (&1) else Cx (&0))) =
     (\x'. if x' = x then Cx(norm (v $$ x) pow 2) else Cx (&0))`
  SUBST1_TAC THENL
  [REWRITE_TAC[FUN_EQ_THM] THEN GEN_TAC THEN
   MESON_TAC[GSYM COMPLEX_NORM_POW_2; COMPLEX_MUL_RID;
             COMPLEX_MUL_RZERO; CX_POW]; ALL_TAC] THEN
  REWRITE_TAC[COMPLEX_VSUM_DELTA; IN_UNIV]);;

let QUANTUM_NORM = prove
 (`!v:complex^(N)multivector.
     norm v = sqrt (sum (:bool^N) (\bs. norm (v$$bs) pow 2))`,
  GEN_TAC THEN
  SUBGOAL_THEN `norm (v:complex^(N)multivector) = sqrt (norm v pow 2)`
  SUBST1_TAC THENL
  [SIMP_TAC[CNORM_POS; GSYM POW_2_SQRT]; ALL_TAC] THEN
  REWRITE_TAC[QUANTUM_SQUARED_NORM]);;

(* ------------------------------------------------------------------------- *)
(* Theorems about linear functions defined on the standard basis.            *)
(* ------------------------------------------------------------------------- *)

let CLINEAR_CBASIS_EXTEND = prove
 (`!f. ?g:complex^M->complex^N.
     clinear g /\
     (!i. 1 <= i /\ i <= dimindex(:M) ==> g (cbasis i) = f i)`,
  REPEAT GEN_TAC THEN
  MP_TAC (SPEC `{cbasis i:complex^M | 1 <= i /\ i <= dimindex(:M)}`
   (ISPEC `\v:complex^M. msum (1..dimindex(:M)) (\i. (v$i) % f i):complex^N`
     CLINEAR_CINDEPENDENT_EXTEND)) THEN
  REWRITE_TAC[CINDEPENDENT_STDCBASIS; FORALL_IN_GSPEC] THEN
  INTRO_TAC "@g. lin g" THEN EXISTS_TAC `g:complex^M->complex^N` THEN
  ASM_REWRITE_TAC[] THEN REPEAT STRIP_TAC THEN ASM_SIMP_TAC[] THEN
  TRANS_TAC EQ_TRANS `msum (1..dimindex(:M))
                        (\j. if j = i then f i else mat 0):complex^N` THEN
  CONJ_TAC THENL [ALL_TAC; ASM_REWRITE_TAC[MSUM_DELTA; IN_NUMSEG]] THEN
  MATCH_MP_TAC MSUM_EQ THEN REWRITE_TAC[IN_NUMSEG; FUN_EQ_THM] THEN
  SIMP_TAC[CBASIS_COMPONENT] THEN REPEAT STRIP_TAC THEN
  COND_CASES_TAC THEN ASM_REWRITE_TAC[] THEN
  REWRITE_TAC[CVECTOR_MUL_ID; CVECTOR_MUL_LZERO]);;

let CLINEAR_QBASIS_EXTEND = prove
 (`!f. ?g:complex^(M)multivector->complex^N.
         clinear g /\ (!bs:bool^M. g (qbasis bs) = f bs)`,
  GEN_TAC THEN
  MP_TAC (ISPEC `\i. f (codebits i:bool^M):complex^N`
    (INST_TYPE [`:(M)multivector`,`:M`] CLINEAR_CBASIS_EXTEND)) THEN
  INTRO_TAC "@g. lin g" THEN
  EXISTS_TAC `g:complex^(M)multivector->complex^N` THEN
  ASM_REWRITE_TAC[QBASIS_CBASIS] THEN GEN_TAC THEN
  ASM_SIMP_TAC[BITSCODE_BOUNDS; DIMINDEX_MULTIVECTOR] THEN
  REWRITE_TAC[CODEBITS_BITSCODE]);;
