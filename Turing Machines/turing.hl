(* ========================================================================= *)
(*                            TURING MACHINES                                *)
(*                                                                           *)
(* (c) Copyright, Andrea Gabrielli, Marco Maggesi 2016-2018                  *)
(* ========================================================================= *)

(* ------------------------------------------------------------------------- *)
(* Configurations (s,x,f)                                                    *)
(*   s = status                                                              *)
(*   x = cursor (natural number)                                             *)
(*   f = tape                                                                *)
(* ------------------------------------------------------------------------- *)

let conf_INDUCT,conf_RECUR = define_type
  "conf = Conf S num (num->B)";;

let CONF_PROJS =
  (end_itlist CONJ o map (new_recursive_definition conf_RECUR))
  [`status (Conf s x f:(A,S)conf) = s`;
   `cursor (Conf s x f:(A,S)conf) = x`;
   `tape (Conf s x f:(A,S)conf) = f`];;

let CONF_EQ = prove
 (`!c c':(A,S)conf.
     c = c' <=>
     status c = status c' /\
     cursor c = cursor c' /\
     tape c = tape c'`,
  GEN_TAC THEN GEN_TAC THEN
  STRUCT_CASES_TAC (ISPEC `c:(A,S)conf` (cases "conf")) THEN
  STRUCT_CASES_TAC (ISPEC `c':(A,S)conf` (cases "conf")) THEN
  REWRITE_TAC[injectivity "conf"; CONF_PROJS]);;

let CONF_ID = prove
 (`!c:(A,S)conf. c = Conf (status c) (cursor c) (tape c)`,
  GEN_TAC THEN REWRITE_TAC[CONF_EQ; CONF_PROJS]);;

(* ------------------------------------------------------------------------- *)
(* Instructions (s,u,v,m,t)                                                  *)
(*   s = initial state                                                       *)
(*   u = current symbol                                                      *)
(*   v = future symbol                                                       *)
(*   m = move                                                                *)
(*   t = final state                                                         *)
(* ------------------------------------------------------------------------- *)

let instr_INDUCT,instr_RECUR = define_type
  "instr = Instr S A A bool S ";;

let INSTR_PROJS =
  (end_itlist CONJ o map (new_recursive_definition instr_RECUR))
  [`init (Instr s u v m t:(A,S)instr) = s`;
   `read (Instr s u v m t:(A,S)instr) = u`;
   `write (Instr s u v m t:(A,S)instr) = v`;
   `move (Instr s u v m t:(A,S)instr) = m`;
   `final (Instr s u v m t:(A,S)instr) = t`];;

(* ------------------------------------------------------------------------- *)
(* Universal and existential quantification on the instructions.             *)
(* ------------------------------------------------------------------------- *)

let FORALL_INSTR = prove
 (`!P. (!i:(A,S)instr. P i) <=> (!s u v m t. P (Instr s u v m t))`,
  MESON_TAC[cases "instr"]);;

let EXISTS_INSTR = prove
 (`!P. (?i:(A,S)instr. P i) <=> (?s u v m t. P (Instr s u v m t))`,
  MESON_TAC[cases "instr"]);;

let INSTR_EQ = prove
 (`!i i':(A,S)instr. i = i' <=> init i = init i' /\
                                read i = read i' /\
                                write i = write i' /\
                                move i = move i' /\
                                final i = final i'`,
  REWRITE_TAC[FORALL_INSTR] THEN REPEAT GEN_TAC THEN
  REWRITE_TAC[INSTR_PROJS; injectivity "instr"]);;

let INSTR_ID = prove
 (`!i:(A,S)instr. i = Instr (init i) (read i) (write i) (move i) (final i)`,
  GEN_TAC THEN REWRITE_TAC[INSTR_EQ; INSTR_PROJS]);;

type_invention_warning := false;;

(* ------------------------------------------------------------------------- *)
(* Specification of Turing Machine.                                          *)
(* Type variables:                                                           *)
(*   :A                         Alphabet                                     *)
(*   :S                         Statuses                                     *)
(*                                                                           *)
(* Variables:                                                                *)
(*   c : (A,S)conf              Configuration                                *)
(*   i : (A,S)instr             Instruction                                  *)
(*   m : (A,S)instr->bool       Machine (list of instructions)               *)
(*   x : num                    Cursor position                              *)
(*   b : bool                   Direction (T = right, F = left)              *)
(*                                                                           *)
(* Functions:                                                                *)
(*   MOVE b                     Move (increment or decrement)                *)
(*   NEXT c i                   Compute next configuration                   *)
(*                                                                           *)
(* Relations:                                                                *)
(*   RELEVANT c i               Relevant instruction                         *)
(*   STEP m c c'                Step of computation                          *)
(*   EXEC m c c'                Multi-step computation                       *)
(*   FINAL m c                  Final configuration                          *)
(*   HALT m c c'                Halt                                         *)
(* ------------------------------------------------------------------------- *)

let RELEVANT_RULES,RELEVANT_INDUCT,RELEVANT_CASES = new_inductive_definition
  `!s x f v m t. RELEVANT (Conf s x f) (Instr s (f x) v m t)`;;

let MOVE = new_recursive_definition bool_RECURSION
  `MOVE T = SUC /\
   MOVE F = PRE`;;

let NEXT = new_definition
  `NEXT c i =
   Conf (final i)
        (MOVE (move i) (cursor c))
        (UPDATE (tape c) (cursor c) (write i))`;;

let STEP_RULES,STEP_INDUCT,STEP_CASES = new_inductive_definition
  `!i c. i IN m /\ RELEVANT c i ==> STEP m c (NEXT c i)`;;

let EXEC = new_definition
  `EXEC m = RTC (STEP m)`;;

let FINAL = new_definition
  `FINAL m c <=> ~(?i. i IN m /\ RELEVANT c i)`;;

let HALT = new_definition
  `HALT m c c' <=> EXEC m c c' /\ FINAL m c'`;;

(* ------------------------------------------------------------------------- *)
(* End of the specification.                                                 *)
(* ------------------------------------------------------------------------- *)

type_invention_warning := true;;

(* ------------------------------------------------------------------------- *)
(* RELEVANT                                                                  *)
(* ------------------------------------------------------------------------- *)

let RELEVANT = prove
 (`!s x f s' u v m t.
     RELEVANT (Conf s x f:(A,S)conf) (Instr s' u v m t) <=> s = s' /\ u = f x`,
  REWRITE_TAC[RELEVANT_CASES; injectivity "conf"; injectivity "instr"] THEN
  MESON_TAC[]);;

let RELEVANT_ALT = prove
 (`!c:(A,S)conf i:(A,S)instr.
     RELEVANT c i <=> status c = init i /\ read i = tape c (cursor c)`,
  GEN_TAC THEN GEN_TAC THEN
  STRUCT_CASES_TAC (ISPEC `c:(A,S)conf` (cases "conf")) THEN
  STRUCT_CASES_TAC (ISPEC `i:(A,S)instr` (cases "instr")) THEN
  REWRITE_TAC[RELEVANT; CONF_PROJS; INSTR_PROJS]);;

(* ------------------------------------------------------------------------- *)
(* STEP                                                                      *)
(* ------------------------------------------------------------------------- *)

let STEP = prove
 (`(!c c':(A,S)conf. ~STEP {} c c') /\
   (!i p c c':(A,S)conf.
      STEP (i INSERT p) c c' <=>
      RELEVANT c i /\ c' = NEXT c i \/ STEP p c c')`,
  REWRITE_TAC[STEP_CASES; NOT_IN_EMPTY; IN_INSERT] THEN MESON_TAC[]);;

let STEP_MONO = prove
 (`!p p' c c':(A,S)conf.
     p SUBSET p' /\ STEP p c c' ==> STEP p' c c'`,
  GEN_TAC THEN GEN_TAC THEN
  CUT_TAC `!c c':(A,S)conf.
    STEP p c c' ==> p SUBSET p' ==> STEP p' c c'` THENL
  [MESON_TAC[]; ALL_TAC] THEN
  MATCH_MP_TAC STEP_INDUCT THEN REPEAT STRIP_TAC THEN
  MATCH_MP_TAC STEP_RULES THEN ASM SET_TAC[]);;

let STEP_UNION_L = prove
 (`!p p' c c':(A,S)conf.
      STEP p c c' ==> STEP (p UNION p') c c'`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN MATCH_MP_TAC STEP_MONO THEN
  EXISTS_TAC `p:(A,S)instr->bool` THEN ASM_REWRITE_TAC[] THEN SET_TAC[]);;

let STEP_UNION_R = prove
 (`!p p' c c':(A,S)conf.
      STEP p c c' ==> STEP (p' UNION p) c c'`,
  REPEAT GEN_TAC THEN STRIP_TAC THEN MATCH_MP_TAC STEP_MONO THEN
  EXISTS_TAC `p:(A,S)instr->bool` THEN
  ASM_REWRITE_TAC[] THEN SET_TAC[]);;

g `!p p' c c':(A,S)conf. STEP p' c c' ==> STEP (p UNION p') c c'`;;
e (REPEAT GEN_TAC THEN STRIP_TAC THEN MATCH_MP_TAC STEP_MONO);;
e (EXISTS_TAC `p':(A,S)instr->bool` THEN CONJ_TAC);;
e (REWRITE_TAC[UNION;SUBSET]);;
e (REWRITE_TAC[IN_ELIM_THM]);;
e (MESON_TAC[]);;
e (ASM_REWRITE_TAC[]);;
let STEP_UNION_R = top_thm();;

let STEP_MAP_STATES = prove
 (`!h:S->T p s1 s2 x1 x2 f1 f2.
     STEP p (Conf s1 x1 f1) (Conf s2 x2 f2)
     ==> STEP {Instr (h s) u v d (h t) | Instr s u v d t IN p}
              (Conf (h s1) x1 f1)
              (Conf (h s2) x2 f2 : (A,T)conf)`,
  REPEAT GEN_TAC THEN ONCE_REWRITE_TAC[STEP_CASES] THEN
  REWRITE_TAC[LEFT_IMP_EXISTS_THM; FORALL_INSTR] THEN REPEAT GEN_TAC THEN
  INTRO_TAC "c i r" THEN
  EXISTS_TAC `Instr (h (s:S)) u v m (h t):(A,T)instr` THEN
  POP_ASSUM_LIST (MP_TAC o CONJ_LIST) THEN
  REWRITE_TAC[RELEVANT; NEXT; CONF_PROJS; INSTR_PROJS; CONF_EQ;
              IN_ELIM_THM; INSTR_EQ] THEN
  MESON_TAC[]);;

(* ------------------------------------------------------------------------- *)
(* FINAL                                                                     *)
(* ------------------------------------------------------------------------- *)

let FINAL_CLAUSES = prove
 (`(!c:(A,S)conf. FINAL {} c) /\
   (!i p c:(A,S)conf. FINAL (i INSERT p) c <=>
                          ~RELEVANT c i /\ FINAL p c)`,
  REWRITE_TAC[FINAL; NOT_IN_EMPTY; IN_INSERT] THEN MESON_TAC[]);;

needs "Library/rstc.ml";;

(* ------------------------------------------------------------------------- *)
(* EXEC.                                                                     *)
(* ------------------------------------------------------------------------- *)

let EXEC_REFL = prove
 (`!p c:(A,S)conf. EXEC p c c`,
  REWRITE_TAC[EXEC; RTC_REFL]);;

let EXEC_EMPTY = prove
 (`!c c':(A,S)conf. EXEC {} c c' <=> c = c'`,
  REPEAT GEN_TAC THEN CUT_TAC `!c c':(A,S)conf. EXEC {} c c' ==> c = c'` THENL
  [MESON_TAC[EXEC_REFL]; ALL_TAC] THEN
   REWRITE_TAC[EXEC] THEN MATCH_MP_TAC RTC_INDUCT_R THEN REWRITE_TAC[STEP]);;

let EXEC_STEP_LEFT = prove
 (`!p c c':(A,S)conf.
     EXEC p c c' <=> c = c' \/ (?b. STEP p c b /\ EXEC p b c')`,
  REPEAT GEN_TAC THEN REWRITE_TAC[EXEC] THEN
  GEN_REWRITE_TAC LAND_CONV [RTC_CASES_R] THEN REFL_TAC);;

let EXEC_UNION_L = prove
 (`!p p' c c':(A,S)conf.
     EXEC p c c' ==> EXEC (p UNION p') c c' `,
  REPEAT GEN_TAC THEN REWRITE_TAC[EXEC; RTC] THEN
  MESON_TAC[RC_MONO; TC_MONO; STEP_UNION_L]);;

let EXEC_UNION_R = prove
 (`!p p' c c':(A,S)conf.
     EXEC p' c c' ==> EXEC (p UNION p') c c' `,
  REPEAT GEN_TAC THEN REWRITE_TAC[EXEC; RTC] THEN
  MESON_TAC[RC_MONO; TC_MONO; STEP_UNION_R]);;

let STEP_IMP_EXEC = prove
 (`!p c1 c2:(A,S)conf. STEP p c1 c2 ==> EXEC p c1 c2`,
  REWRITE_TAC[EXEC; RTC_INC]);;

let EXEC_TRANS = prove
 (`!p c1 c2 c3:(A,S)conf. EXEC p c1 c2 /\ EXEC p c2 c3 ==> EXEC p c1 c3`,
  REPEAT GEN_TAC THEN REWRITE_TAC[EXEC] THEN STRIP_TAC THEN
  TRANS_TAC RTC_TRANS `c2:(A,S)conf` THEN ASM_REWRITE_TAC[]);;

let EXEC_INDUCT = prove
 (`!P p. (!c:(A,S)conf. P c c) /\
         (!c1 c2. STEP p c1 c2 ==> P c1 c2) /\
         (!c1 c2 c3. P c1 c2 /\ P c2 c3 ==> P c1 c3)
         ==> (!c1 c2. EXEC p c1 c2 ==> P c1 c2)`,
  GEN_TAC THEN GEN_TAC THEN STRIP_TAC THEN REWRITE_TAC[EXEC] THEN
  MATCH_MP_TAC RTC_INDUCT THEN ASM_REWRITE_TAC[]);;

let EXEC_MONO = prove
 (`!p1 p2 c1 c2:(A,S)conf. p1 SUBSET p2 /\ EXEC p1 c1 c2 ==> EXEC p2 c1 c2`,
  GEN_TAC THEN GEN_TAC THEN REWRITE_TAC[IMP_CONJ; RIGHT_FORALL_IMP_THM] THEN
  DISCH_TAC THEN MATCH_MP_TAC EXEC_INDUCT THEN
  REWRITE_TAC[EXEC_REFL; EXEC_TRANS] THEN REPEAT STRIP_TAC THEN
  MATCH_MP_TAC STEP_IMP_EXEC THEN MATCH_MP_TAC STEP_MONO THEN
  ASM_MESON_TAC[]);;

let EXEC_MAP_STATES = prove
 (`!h p s1 s2:S x1 x2 f1 f2.
     EXEC p (Conf s1 x1 f1) (Conf s2 x2 f2)
     ==> EXEC {Instr (h s:T) (u:A) v m (h t) | Instr s u v m t IN p}
              (Conf (h s1) x1 f1)
              (Conf (h s2) x2 f2)`,
  REPEAT GEN_TAC THEN DISCH_TAC THEN SUBGOAL_THEN
    `!c1 c2:(A,S)conf.
       EXEC p c1 c2
       ==> EXEC {Instr (h s:T) u v m (h t) | Instr s u v m t IN p}
                (Conf (h (status c1)) (cursor c1) (tape c1))
                (Conf (h (status c2)) (cursor c2) (tape c2))`
     (MATCH_MP_TAC o REWRITE_RULE[CONF_PROJS] o
      SPECL[`Conf s1 x1 f1:(A,S)conf`; `Conf s2 x2 f2:(A,S)conf`]) THENL
  [ALL_TAC; ASM_REWRITE_TAC[]] THEN
  MATCH_MP_TAC EXEC_INDUCT THEN REWRITE_TAC[EXEC_REFL; EXEC_TRANS] THEN
  REPEAT STRIP_TAC THEN MATCH_MP_TAC STEP_IMP_EXEC THEN
  MATCH_MP_TAC STEP_MAP_STATES THEN ASM_REWRITE_TAC[GSYM CONF_ID]);;

(* ------------------------------------------------------------------------- *)
(* Evaluation strategy for non-deterministic machines (RUN).                 *)
(* ------------------------------------------------------------------------- *)

let RUN_DEF = new_definition
  `!p a c c':(A,S)conf. RUN p a c c' <=>
                        c = c' \/ (?b. STEP a c b /\ EXEC p b c')`;;

let RUN_REFL = prove
 (`!p a c:(A,S)conf. RUN p a c c`,
  REWRITE_TAC[RUN_DEF]);;

g `!p a c c':(A,S)conf.
      a SUBSET p /\ RUN p a c c' ==> EXEC p c c'`;;
e (ONCE_REWRITE_TAC[RUN_DEF; EXEC_STEP_LEFT]);;
e (REPEAT STRIP_TAC);;
e (POP_ASSUM (SUBST1_TAC o GSYM));;
e (REWRITE_TAC[RUN_REFL]);;
e (DISJ2_TAC);;
e (EXISTS_TAC `b:(A,S)conf`);;
e (ASM_REWRITE_TAC[]);;
e (MATCH_MP_TAC STEP_MONO);;
e (EXISTS_TAC `a:(A,S)instr -> bool`);;
e (ASM_REWRITE_TAC[]);;
let RUN_IMP_EXEC = top_thm();;

g `!p c c':(A,S)conf. EXEC p c c' ==> RUN p p c c'`;;
e (REPEAT GEN_TAC);;
e (ONCE_REWRITE_TAC[RUN_DEF; EXEC_STEP_LEFT]);;
e (REWRITE_TAC[EXEC_STEP_LEFT]);;
let EXEC_IMP_RUN_instr = top_thm();;

g `!p c c':(A,S)conf. EXEC p c c' <=> RUN p p c c'`;;
e (REPEAT GEN_TAC);;
e (EQ_TAC);;
e (REPEAT STRIP_TAC);;
e (ASM_SIMP_TAC[EXEC_IMP_RUN_instr]);;
e (ONCE_REWRITE_TAC[RUN_DEF; EXEC_STEP_LEFT]);;
e (REWRITE_TAC[EXEC_STEP_LEFT]);;
let EXEC_EQ_RUN = top_thm();;

(* ------------------------------------------------------------------------- *)
(* RUN_CLAUSES                                                               *)
(* ------------------------------------------------------------------------- *)

g `(!p c c':(A,S)conf. RUN p {} c c' <=> c = c') /\
   (!i p a c c':(A,S)conf.
      RUN p (i INSERT a) c c' <=>
      RELEVANT c i /\ RUN p p (NEXT c i) c' \/ RUN p a c c')`;;
e CONJ_TAC;;
e (REWRITE_TAC[RUN_DEF; STEP]);;
e (REPEAT GEN_TAC);;
e (GEN_REWRITE_TAC LAND_CONV [RUN_DEF]);;
e (ASM_CASES_TAC `c = c':(A,S)conf` THEN ASM_REWRITE_TAC[RUN_REFL]);;
e EQ_TAC;;
e (INTRO_TAC "@b. s e");;
e (REMOVE_THEN "s" (STRIP_ASSUME_TAC o REWRITE_RULE[STEP]));;
e (ASM_REWRITE_TAC[]);;
e (POP_ASSUM SUBST_VAR_TAC);;
e (ASM_REWRITE_TAC[GSYM EXEC_EQ_RUN]);;
e DISJ2_TAC;;
e (ASM_REWRITE_TAC[RUN_DEF]);;
e (EXISTS_TAC `b:(A,S)conf`);;
e (ASM_REWRITE_TAC[]);;
e (STRIP_TAC);;
e (EXISTS_TAC `NEXT (c:(A,S)conf) (i:(A,S)instr) :(A,S)conf`);;
e (ASM_REWRITE_TAC[EXEC_EQ_RUN; STEP]);;
e (POP_ASSUM (MP_TAC o REWRITE_RULE[RUN_DEF]));;
e (ASM_REWRITE_TAC[]);;
e STRIP_TAC;;
e (EXISTS_TAC `b:(A,S)conf`);;
e (ASM_REWRITE_TAC[STEP]);;
let RUN_CLAUSES = top_thm();;

(* ------------------------------------------------------------------------- *)
(* Deterministic Turing machines.                                            *)
(* ------------------------------------------------------------------------- *)

let DETERMINISTIC = new_definition
  `DETERMINISTIC p <=>
   (!i j:(A,S)instr.
      i IN p /\ j IN p /\ init i = init j /\ read i = read j
      ==> i = j)`;;

let DETERMINISTIC_PAIRWISE = prove
 (`!p. DETERMINISTIC p <=>
       pairwise (\i j:(A,S)instr. ~(init i = init j) \/ ~(read i = read j)) p`,
  REWRITE_TAC[pairwise; DETERMINISTIC] THEN MESON_TAC[]);;

let DETERMINISTIC_EMPTY = prove
 (`DETERMINISTIC ({}:(A,S)instr->bool)`,
  REWRITE_TAC[DETERMINISTIC_PAIRWISE; PAIRWISE_EMPTY]);;

let DETERMINISTIC_INSERT = prove
 (`!p a:(A,S)instr. DETERMINISTIC (a INSERT p) <=>
                    DETERMINISTIC p /\
                    (!y. y IN p /\ ~(y = a)
                         ==> ~(init a = init y) \/ ~(read a = read y))`,
  REWRITE_TAC[DETERMINISTIC_PAIRWISE; PAIRWISE_INSERT] THEN MESON_TAC[]);;

let DETERM = new_definition
  `DETERM (R:A->B->bool) s <=> !x y z. x IN s /\ R x y /\ R x z ==> y = z`;;

let DETERMINISTIC_IMP_DETERM_STEP = prove
 (`!p. DETERMINISTIC p ==> DETERM (STEP p) (:(A,S)conf)`,
  REWRITE_TAC[DETERMINISTIC; DETERM; IN_UNIV; STEP_CASES] THEN
  INTRO_TAC "!p; hp; !x y z; (@i. y i hi) (@j. z j hj)" THEN
  REPEAT (FIRST_X_ASSUM SUBST_VAR_TAC) THEN AP_TERM_TAC THEN
  FIRST_X_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[] THEN
  REMOVE_THEN "hi" MP_TAC THEN REMOVE_THEN "hj" MP_TAC THEN
  SIMP_TAC[RELEVANT_ALT]);;

(* ------------------------------------------------------------------------- *)
(* Deterministic execution.                                                  *)
(* ------------------------------------------------------------------------- *)

let FSTEP = new_recursive_definition list_RECURSION
  `(!c:(A,S)conf. FSTEP [] c = NONE) /\
   (!i p c. FSTEP (CONS i p) c =
            if RELEVANT c i then SOME (NEXT c i) else FSTEP p c)`;;

let FSTEP_IMP_STEP = prove
 (`!p c c':(A,S)conf. FSTEP p c = SOME c' ==> STEP (set_of_list p) c c'`,
  FIX_TAC "c c'" THEN MATCH_MP_TAC list_INDUCT THEN
  REWRITE_TAC[FSTEP; distinctness "option"; set_of_list; STEP] THEN
  INTRO_TAC "![i] [p]; hp" THEN COND_CASES_TAC THEN
  ASM_SIMP_TAC[injectivity "option"]);;

let STEP_IMP_FSTEP = prove
 (`!p c c':(A,S)conf.
     DETERMINISTIC (set_of_list p) /\ STEP (set_of_list p) c c'
     ==> FSTEP p c = SOME c'`,
  FIX_TAC "c c'" THEN MATCH_MP_TAC list_INDUCT THEN
  REWRITE_TAC[FSTEP; distinctness "option"; set_of_list;
              DETERMINISTIC; STEP] THEN
  INTRO_TAC "![i] [p]" THEN
  REPEAT STRIP_TAC THEN ASM_REWRITE_TAC[] THEN
  COND_CASES_TAC THENL
  [REWRITE_TAC[injectivity "option"] THEN
   FIRST_X_ASSUM (MP_TAC o GEN_REWRITE_RULE I [STEP_CASES]) THEN
   STRIP_TAC THEN FIRST_X_ASSUM SUBST_VAR_TAC THEN
   AP_TERM_TAC THEN FIRST_X_ASSUM MATCH_MP_TAC THEN
   ASM_REWRITE_TAC[IN_INSERT] THEN ASM_MESON_TAC[RELEVANT_ALT];
   FIRST_X_ASSUM MATCH_MP_TAC THEN ASM_REWRITE_TAC[] THEN
   REPEAT STRIP_TAC THEN FIRST_X_ASSUM MATCH_MP_TAC THEN
   ASM_REWRITE_TAC[IN_INSERT]]);;

let FSTEP_EQ_STEP = prove
 (`!p c c':(A,S)conf. DETERMINISTIC (set_of_list p)
                      ==> (FSTEP p c = SOME c' <=> STEP (set_of_list p) c c')`,
  MESON_TAC[FSTEP_IMP_STEP; STEP_IMP_FSTEP]);;

let FEXEC_RULES,FEXEC_INDUCT,FEXEC_CASE = new_inductive_definition
  `(!p c:(A,S)conf. FEXEC p c c) /\
   (!p c b c'. FSTEP p c = SOME b /\ FEXEC p b c' ==> FEXEC p c c')`;;

let FEXEC_REFL,FEXEC_FSTEP = CONJ_PAIR FEXEC_RULES;;

let FEXEC_IMP_EXEC = prove
 (`!p c c':(A,S)conf. FEXEC p c c' ==> EXEC (set_of_list p) c c'`,
   MATCH_MP_TAC FEXEC_INDUCT THEN REWRITE_TAC[EXEC_REFL] THEN
   REPEAT STRIP_TAC THEN REWRITE_TAC[EXEC] THEN MATCH_MP_TAC RTC_TRANS_R THEN
   EXISTS_TAC `b:(A,S)conf` THEN ASM_REWRITE_TAC[GSYM EXEC] THEN
   MATCH_MP_TAC FSTEP_IMP_STEP THEN ASM_REWRITE_TAC[]);;

let EXEC_IMP_FEXEC = prove
 (`!p c c':(A,S)conf.
     DETERMINISTIC (set_of_list p) /\ EXEC (set_of_list p) c c'
     ==> FEXEC p c c'`,
  GEN_TAC THEN REWRITE_TAC[IMP_CONJ; RIGHT_FORALL_IMP_THM] THEN
  DISCH_TAC THEN REWRITE_TAC[EXEC] THEN MATCH_MP_TAC RTC_INDUCT_R THEN
  REWRITE_TAC[FEXEC_REFL] THEN REPEAT STRIP_TAC THEN
  MATCH_MP_TAC FEXEC_FSTEP THEN EXISTS_TAC `c':(A,S)conf` THEN
  ASM_REWRITE_TAC[] THEN MATCH_MP_TAC STEP_IMP_FSTEP THEN
  ASM_REWRITE_TAC[]);;

let FEXEC_EQ_EXEC = prove
 (`!p c c':(A,S)conf.
      DETERMINISTIC (set_of_list p)
      ==> (FEXEC p c c' <=> EXEC (set_of_list p) c c')`,
  MESON_TAC[EXEC_IMP_FEXEC; FEXEC_IMP_EXEC]);;

let EXEC_EQ_FEXEC = prove
 (`!p c c':(A,S)conf.
      FINITE p /\ DETERMINISTIC p
      ==> (EXEC p c c' <=> FEXEC (list_of_set p) c c')`,
  REPEAT STRIP_TAC THEN
  SUBGOAL_THEN
    `p = set_of_list (list_of_set p):(A,S)instr->bool`
    (fun th -> GEN_REWRITE_TAC (LAND_CONV o RATOR_CONV o LAND_CONV) [th]) THENL
  [ASM_SIMP_TAC[SET_OF_LIST_OF_SET]; ALL_TAC] THEN
  MATCH_MP_TAC EQ_SYM THEN MATCH_MP_TAC FEXEC_EQ_EXEC THEN
  ASM_SIMP_TAC[SET_OF_LIST_OF_SET]);;

let FROM_OPTION = new_recursive_definition option_RECURSION
  `(!f:A->B d. FROM_OPTION f d NONE = d) /\
   (!f d a. FROM_OPTION f d (SOME a) = f a)`;;

let FEXEC_CLAUSES = prove
 (`!p c c':(A,S)conf.
     FEXEC p c c' <=> c = c' \/
                      FROM_OPTION (\b. FEXEC p b c') F (FSTEP p c)`,
  REPEAT GEN_TAC THEN GEN_REWRITE_TAC LAND_CONV [FEXEC_CASE] THEN
  ASM_CASES_TAC `c = c':(A,S)conf` THEN ASM_REWRITE_TAC[] THEN
  EQ_TAC THENL [STRIP_TAC THEN ASM_REWRITE_TAC[FROM_OPTION]; ALL_TAC] THEN
  STRUCT_CASES_TAC (ISPEC `FSTEP p (c:(A,S)conf)` (cases "option")) THEN
  REWRITE_TAC[FROM_OPTION; distinctness "option"; injectivity "option"] THEN
  MESON_TAC[]);;
