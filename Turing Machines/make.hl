(* ========================================================================= *)
(*          TURING MACHINES AND COMPUTABILITY THEORY                         *)
(*                                                                           *)
(* (c) Copyright, Andrea Gabrielli, Marco Maggesi 2016-2018                  *)
(* ========================================================================= *)

needs "Library/iter.ml";;
needs "Library/rstc.ml";;
(*needs "Snippets/compute.hl";;*)

loadt"Turing Machines/compute.hl";;
type_invention_warning := true;;

loadt "Turing Machines/fixprinter.hl";;   (* Fix for the HOL Light printer.*)

loadt "Turing Machines/misc.hl";;         (* Miscellanea *)

loadt "Turing Machines/turing.hl";;       (* General specification *)       

loadt "Turing Machines/numtape.hl";;      (* `:num->Sym` tape *)

loadt "Turing Machines/computability.hl";;   
      (* Computable functions `:num->num` *)

loadt "Turing Machines/composition.hl";;        
      (* Composition of computable functions *)

loadt "Turing Machines/examples.hl";;           
      (* Example of execution *)

loadt "Turing Machines/run.hl";; (* Automatic execution of a Turing machine  *)