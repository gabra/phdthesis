===============================================
QUATERNIONIC ANALYSIS IN HOL LIGHT
===============================================

(c) Copyright, Andrea Gabrielli, Marco Maggesi 2016-2017

The directory "Application of Quaternions" contains some related 
formalization about some applications of the library about quaternion 
for the HOL Light theorem prover.

The code is divided in three subdirectory

- Complex

  Prerequisites about real and complex analysis which are not strictly
  related to quaternions:
    - Limit superior and limit inferior.
    - Root test for series.
    - Cauchy-Hadamard formula for the radius of convergence.
  Included in HOL Light (Mon 10th April 2017).

- Slice_Regular

  Definition of Gentili's and Struppa's slice regular functions.
  Proof of the equivalence between regularity and the existence of 
  series expansion in the origin.

- PH_curves

  Definition of Farouki's Pythagorean-Hodograph curves.  Quaternionic
  representation of spacial PH curvers.  Cubic and quintic PH Hermite
  interpolation curves.

===============================================
TURING MACHINES IN HOL LIGHT
===============================================

(c) Copyright, Andrea Gabrielli, Marco Maggesi 2016-2018

The subdirectory "Turing Machines" contains some related 
formalization about Turing machines for the HOL Light theorem prover.

- Definition of Turing Machine

- Certified execution of Turing machines

- Basics about computability theory 
    - definition of function Turing computable (only for arity 1)
    - proof of the conservation of turing computability under 
      composition
    - example of functions Turing computable (successor function etc.)

===============================================
QUANTUM COMPUTING IN HOL LIGHT
===============================================

(c) Copyright, Andrea Gabrielli, Marco Maggesi 2016-2018

The subdirectory "Quantum computing" contains some related 
formalization about quantum computing for the HOL Light theorem prover 
and the related needed formal theories.

The code is divided in three subdirectories.

Fintypes and Finvec

- formal thoery about types with finite cardinality and vectors indexed 
  by such types

Complex Vector

- formal theory about complex vectors and complex linear algebra.
  (developed starting form the HOL Light file "cvector.hl" written
   by Sanaz Khan Afshar \& Vincent Aravantinos, 2011-13)


Quantum 

- states of Qbits and quantum registers

- definition and certified computation of quantum logical gates

- examples of certified execution of some fundamental quantum 
  algorithms and protocols (teleportation and superdense coding protocols
  and the Deutsch's algorithm)


