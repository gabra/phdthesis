(* ========================================================================= *)
(*                     QUATERNIONIC POWER SERIES                             *)
(*                                                                           *)
(* Copyright (c) 2016 Marco Maggesi - Andrea Gabrielli                       *)
(* ========================================================================= *)

(* ------------------------------------------------------------------------- *)
(* Absolute and simple convergence.                                          *)
(* ------------------------------------------------------------------------- *)

let QUAT_ABSCONV_POWER_SERIES = prove
 (`!q a k b.
      ((\n. root n (norm (a n:quat))) has_limsup b) (sequentially within k) /\
      b * norm q < &1
      ==> real_summable k (\n. norm (q pow n * a n)) `,
  REPEAT GEN_TAC THEN ONCE_REWRITE_TAC[GSYM REAL_ABS_NORM] THEN
  INTRO_TAC "lim radius" THEN
  ASM_CASES_TAC `FINITE (k:num->bool)` THENL
  [ASM_SIMP_TAC[REAL_SUMMABLE_FINITE]; POP_ASSUM (LABEL_TAC "fin")] THEN
  MATCH_MP_TAC REAL_SERIES_ROOT_TEST THEN
  CONV_TAC (REWRITE_CONV[QUAT_NORM_MUL; REAL_MUL_SYM]) THEN
  CONV_TAC (REWRITE_CONV[QUAT_NORM_POW]) THEN
  EXISTS_TAC `(b:real) * abs (norm (q:real^4))` THEN
  ASM_REWRITE_TAC[REAL_ABS_POS] THEN
  REWRITE_TAC[REAL_ABS_MUL; REAL_ROOT_MUL] THEN
  MATCH_MP_TAC HAS_LIMSUP_MUL_REALLIM_RIGHT THEN
  ASM_SIMP_TAC[EVENTUALLY_TRUE; REAL_ABS_POS; REAL_ABS_POW; ROOT_POS_LE;
               REAL_ABS_NORM] THEN
  MATCH_MP_TAC REALLIM_TRANSFORM_EVENTUALLY THEN
  EXISTS_TAC `\n:num. norm (q:quat)` THEN
  ASM_REWRITE_TAC[REALLIM_CONST; EVENTUALLY_SEQUENTIALLY_WITHIN ] THEN
  EXISTS_TAC `1` THEN INTRO_TAC "!n; hp" THEN
  ASM_SIMP_TAC[REAL_ROOT_POW; NORM_POS_LE;
               ARITH_RULE`1 <= n ==> ~(n = 0)`]);;

let QUAT_CONV_POWER_SERIES = prove
 (`!a k b q:quat.
     ((\n. root n (norm (a n))) has_limsup b) (sequentially within k) /\
     b * norm q < &1
     ==> summable k (\n. q pow n * a n)`,
  REPEAT GEN_TAC THEN INTRO_TAC "lim radius" THEN
  MATCH_MP_TAC SERIES_NORMCONV_IMP_CONV THEN
  ASM_MESON_TAC[QUAT_ABSCONV_POWER_SERIES]);;

let QUAT_CONV_POWER_SERIES_POINTWISE = prove
 (`!a k b q.
     ((\n:num. root n (norm (a n:quat))) has_limsup b)
      (sequentially within k) /\
     b * norm q < &1
     ==> ?l:quat. ((\n. q pow n * a n) sums l) k`,
  ACCEPT_TAC (REWRITE_RULE [summable] QUAT_CONV_POWER_SERIES));;

(* ------------------------------------------------------------------------- *)
(* Convergence and absolute convergence of                                   *)
(* derivative quaternionic power series                                      *)
(* ------------------------------------------------------------------------- *)

let QUAT_ABSCONV_POWER_SERIES_DERIVATIVE = prove
 (`!q a k b.
     ((\n. root n (norm (a n:quat))) has_limsup b) (sequentially within k) /\
     b * norm q < &1
     ==> real_summable k (\n. norm (q pow (n - 1) * Hx (&n) * a n))`,
  REPEAT GEN_TAC THEN INTRO_TAC "limsup norm" THEN
  ASM_CASES_TAC `FINITE (k:num->bool)` THENL
  [ASM_SIMP_TAC[REAL_SUMMABLE_FINITE]; POP_ASSUM (LABEL_TAC "fin")] THEN
  MATCH_MP_TAC (REAL_SERIES_ROOT_TEST) THEN
  EXISTS_TAC `b * norm (q:quat)` THEN ASM_REWRITE_TAC[NORM_POS_LE] THEN
  ASM_CASES_TAC `norm (q:quat) = &0` THENL
  [ASM_REWRITE_TAC[QUAT_NORM_MUL; NORM_HX; REAL_ROOT_MUL; QUAT_NORM_POW] THEN
   MATCH_MP_TAC HAS_LIMSUP_TRANSFORM THEN EXISTS_TAC `\n:num. &0` THEN
   CONJ_TAC THENL
   [ASM_REWRITE_TAC[EVENTUALLY_SEQUENTIALLY_WITHIN] THEN
    EXISTS_TAC `2` THEN INTRO_TAC "!n; n" THEN
    CLAIM_TAC "n1 n2" `~(n = 0) /\ ~(n - 1 = 0)` THENL
    [ASM_ARITH_TAC; ALL_TAC] THEN
    ASM_SIMP_TAC[REAL_POW_ZERO; ARITH_RULE`2 <= n ==> ~ (n = 0)`;
                 ROOT_0; REAL_MUL_LZERO];
    ASM_SIMP_TAC[REAL_MUL_RZERO] THEN MATCH_MP_TAC REALLIM_IMP_HAS_LIMSUP THEN
    ASM_SIMP_TAC[REALLIM_CONST]];
    CLAIM_TAC "sp" `&0 < norm (q:quat)` THENL
    [ASM_MESON_TAC[NORM_POS_LE; REAL_ARITH`!x. &0 <= x <=> x = &0 \/
                                               &0 < x`]; ALL_TAC] THEN
    ASM_REWRITE_TAC[QUAT_NORM_MUL; NORM_HX; REAL_ROOT_MUL; QUAT_NORM_POW] THEN
    SUBGOAL_THEN `b * norm (q:quat)  = norm q * &1 * b` SUBST_ALL_TAC THENL
    [REAL_ARITH_TAC; ALL_TAC] THEN
    REWRITE_TAC[REAL_MUL_ASSOC] THEN REWRITE_TAC[REAL_MUL_SYM] THEN
    MATCH_MP_TAC HAS_LIMSUP_MUL_REALLIM_RIGHT THEN
    REPEAT CONJ_TAC THENL
    [ASM_REWRITE_TAC[];
     MATCH_MP_TAC REALLIM_MUL THEN
     CONJ_TAC THENL
     [MATCH_MP_TAC REALLIM_SEQUENTIALLY_WITHIN THEN
      REWRITE_TAC[REAL_ABS_NUM; REALLIM_ROOT_REFL];
      MATCH_MP_TAC REALLIM_TRANSFORM_EVENTUALLY THEN
      EXISTS_TAC `\n:num. (norm (q:quat)) rpow (&1 - inv(&n))` THEN
      CONJ_TAC THENL
      [ASM_REWRITE_TAC[EVENTUALLY_SEQUENTIALLY_WITHIN] THEN
       EXISTS_TAC `2` THEN INTRO_TAC "!n; n" THEN
       CLAIM_TAC "n1 n2" `~(n = 0) /\ ~(n - 1 = 0)` THENL
       [ASM_ARITH_TAC; ALL_TAC] THEN
       REWRITE_TAC[GSYM QUAT_NORM_POW] THEN
       ASM_SIMP_TAC[NORM_POS_LE; REAL_ROOT_RPOW] THEN
       REWRITE_TAC[QUAT_NORM_POW;GSYM RPOW_POW] THEN
       ASM_SIMP_TAC[GSYM REAL_OF_NUM_SUB; ARITH_RULE`2 <= n ==> 1 <= n`] THEN
       ASM_SIMP_TAC[NORM_POS_LE; RPOW_RPOW;real_div] THEN
       AP_TERM_TAC THEN REWRITE_TAC[REAL_MUL_LID;REAL_SUB_RDISTRIB] THEN
       USE_THEN "n1" MP_TAC THEN REWRITE_TAC[GSYM REAL_OF_NUM_EQ] THEN
       ASM_SIMP_TAC[REAL_MUL_RINV];
       SUBGOAL_THEN `!f:num->real a. (f ---> a) = (f ---> a rpow &1)`
         (fun th -> ONCE_REWRITE_TAC[th]) THENL
       [REWRITE_TAC[RPOW_POW; REAL_POW_1]; ALL_TAC] THEN
       MATCH_MP_TAC REALLIM_RPOW_COMPOSE THEN
       ASM_REWRITE_TAC[] THEN
       ASM_REWRITE_TAC[REALLIM_CONST] THEN
       MATCH_MP_TAC REALLIM_SEQUENTIALLY_WITHIN THEN
       CONV_TAC (RATOR_CONV( RAND_CONV(
                 ONCE_REWRITE_CONV[REAL_ARITH`&1 = &1 - &0`]))) THEN
       MATCH_MP_TAC REALLIM_SUB THEN
       REWRITE_TAC[REALLIM_CONST;REALLIM_1_OVER_N]]];
       ASM_REWRITE_TAC[EVENTUALLY_SEQUENTIALLY_WITHIN] THEN
       EXISTS_TAC `1` THEN INTRO_TAC "!n; n" THEN
       MATCH_MP_TAC ROOT_POS_LE THEN REWRITE_TAC[NORM_POS_LE];
       ASM_REWRITE_TAC[EVENTUALLY_SEQUENTIALLY_WITHIN] THEN EXISTS_TAC `1` THEN
       INTRO_TAC "!n; n" THEN MATCH_MP_TAC REAL_LE_MUL THEN
       REWRITE_TAC[GSYM QUAT_NORM_POW] THEN
       MESON_TAC[NORM_POS_LE; ROOT_POS_LE;REAL_ABS_POS]]]);;

let QUAT_CONV_POWER_SERIES_DERIVATIVE = prove
 (`!q a k b.
     ((\n. root n (norm (a n))) has_limsup b) (sequentially within k) /\
     b * norm q < &1
     ==> summable k (\n. q pow (n - 1) * Hx (&n) * a n)`,
  REPEAT GEN_TAC THEN INTRO_TAC "limsup pos" THEN
  MATCH_MP_TAC (SERIES_NORMCONV_IMP_CONV) THEN
  ASM_MESON_TAC[QUAT_ABSCONV_POWER_SERIES_DERIVATIVE]);;

(* ------------------------------------------------------------------------- *)
(* Usefull compact sets properties.                                          *)
(* ------------------------------------------------------------------------- *)

let COMPACT_SUBSET_BALL_RESTRICT = prove
 (`!s x:real^N r. ~(s = {}) /\ compact s /\ s SUBSET ball(x,r)
               ==> ?r'. &0 < r' /\ r' < r /\ s SUBSET ball(x,r')`,
  GEOM_ORIGIN_TAC `x:real^N` THEN REPEAT GEN_TAC THEN
  REWRITE_TAC[SUBSET; IN_BALL_0] THEN INTRO_TAC "ne cpt sub" THEN
  MP_TAC (SPEC `IMAGE norm (s:real^N->bool)` COMPACT_ATTAINS_SUP) THEN
  ANTS_TAC THENL
  [ASM_REWRITE_TAC[IMAGE_EQ_EMPTY; GSYM IMAGE_o] THEN
   MATCH_MP_TAC COMPACT_CONTINUOUS_IMAGE THEN
   ASM_REWRITE_TAC[CONTINUOUS_ON_LIFT_NORM];
   ALL_TAC] THEN
  REWRITE_TAC[EXISTS_IN_IMAGE; FORALL_IN_IMAGE] THEN
  INTRO_TAC "@y. y le" THEN
  EXISTS_TAC `(norm (y:real^N) + r) / &2` THEN
  HYP_TAC "sub: +" (ISPEC `y:real^N`) THEN ASM_REWRITE_TAC[] THEN
  INTRO_TAC "lt" THEN REPEAT CONJ_TAC THEN TRY ASM_NORM_ARITH_TAC THEN
  GEN_TAC THEN DISCH_THEN (HYP_TAC "le: +" o C MATCH_MP) THEN
  ASM_REAL_ARITH_TAC);;

(* ------------------------------------------------------------------------- *)
(* Uniform convergence.                                                      *)
(* ------------------------------------------------------------------------- *)

parse_as_infix ("uniformly_convergent_on",(12,"right"));;

let uniformly_convergent_on = new_definition
  `((f:B->A->real^N) uniformly_convergent_on (s:A->bool)) net <=>
   ?l:A->real^N. !e.
     &0 < e ==> eventually (\n:B. !x. x IN s ==> dist (f n x,l x) < e) net`;;

parse_as_infix ("uniformly_summable_on",(12,"right"));;

let uniformly_summable_on = new_definition
 `((f:num->A->real^N) uniformly_summable_on (s:A->bool)) k <=>
  ((\n x. vsum (k INTER (0..n)) (\i. f i x)) uniformly_convergent_on s)
  (sequentially)`;;

(* ------------------------------------------------------------------------- *)
(* Weiestrass criterion for uniform convegence                               *)
(* ------------------------------------------------------------------------- *)

let WEIESTRASS_SERIES_COMPARISON_UNIFORM = prove
 (`!f:num->A->real^N M s k.
     (!x. x IN s ==> (!n. norm (f n x) <= M n)) /\
     (?l. (M real_sums l) k)
     ==> (f uniformly_summable_on s) k`,
  INTRO_TAC "!f M s k; bound (@l. sum)" THEN
  (MP_TAC o BETA_RULE o
   ISPECL[`\x:A n:num. f n x:real^N`;`M:num->real`;
          `\x:A. x IN s`;`k:num->bool`])
     SERIES_COMPARISON_UNIFORM THEN
  ANTS_TAC THENL
  [ASM_SIMP_TAC[] THEN EXISTS_TAC `lift l` THEN
   ASM_REWRITE_TAC[GSYM REAL_SUMS];
   ALL_TAC] THEN
  INTRO_TAC "@l. hp" THEN
  REWRITE_TAC[uniformly_summable_on; uniformly_convergent_on;
              EVENTUALLY_SEQUENTIALLY] THEN
  ASM_MESON_TAC[]);;

let UNIFORMLY_SUMMABLE_ON_SUBSET = prove
  (`!f:num->A->real^N s t k.
       (f uniformly_summable_on s) k /\ t SUBSET s
       ==> (f uniformly_summable_on t) k`,
   REWRITE_TAC[SUBSET; uniformly_summable_on; uniformly_convergent_on;
               EVENTUALLY_SEQUENTIALLY] THEN
   ASM_MESON_TAC[]);;

let UNIFORM_CONV_IMP_SUMMABLE = prove
 (`!f:num->A->real^N s k.
    (f uniformly_summable_on s) k
    ==> (!x. x IN s ==> summable k (\n. f n x))`,
  REPEAT GEN_TAC THEN REWRITE_TAC[summable;sums;uniformly_convergent_on;
                                  uniformly_summable_on; tendsto;
                                  EVENTUALLY_SEQUENTIALLY]  THEN
  MESON_TAC[]);;

let UNIFORM_CONV_IMP_REAL_SUMMABLE = prove
 (`!f:num->A->real s k.
    ((\n x. lift (f n x)) uniformly_summable_on s) k
    ==> (!x. x IN s ==> real_summable k (\n. f n x))`,
  REPEAT GEN_TAC THEN
  REWRITE_TAC[real_summable; real_sums;uniformly_convergent_on; tendsto_real;
              uniformly_summable_on; EVENTUALLY_SEQUENTIALLY]  THEN
  REWRITE_TAC[DIST_1; SUM_VSUM; o_DEF] THEN
  MESON_TAC[]);;

(* ------------------------------------------------------------------------- *)
(* Power series and their derivative uniform convergence.                    *)
(* ------------------------------------------------------------------------- *)

let QUAT_UNIFORM_CONV_POWER_SERIES = prove
 (`!a b s k.
     ((\n. root n (norm (a n:quat))) has_limsup b) (sequentially within k) /\
     compact s /\
     (!q. q IN s ==> b * norm q < &1)
     ==>
     ((\i q:quat. q pow i * a i) uniformly_summable_on s) k`,
  REWRITE_TAC[uniformly_summable_on; uniformly_convergent_on;
              EVENTUALLY_SEQUENTIALLY] THEN
  REWRITE_TAC[
   MESON[]`(?l:quat->quat. !e. &0 < e
                  ==> (?N:num. !n. N <= n
                               ==> (!x:quat. x IN s
                                        ==> dist
                                            (vsum (k INTER (0..n))
                                             (\i. x pow i * a i),
                                             l x) <
                                            e)))
           <=>
           (?l:quat->quat. !e. &0 < e
                   ==> ?N:num. !n q:quat. N <= n /\ q IN s
                                 ==> dist(vsum (k INTER (0..n))
                                  (\i. q pow i * a i),l q) < e)`] THEN
  INTRO_TAC "!a b s k; limsup cpt sub" THEN
  ASM_CASES_TAC `FINITE (k:num->bool)` THENL
  [POP_ASSUM MP_TAC THEN REWRITE_TAC[num_FINITE] THEN
   INTRO_TAC "@N. N" THEN
   EXISTS_TAC `\q:quat. vsum k (\i. q pow i * a i)` THEN
   INTRO_TAC "!e; epos" THEN EXISTS_TAC `N:num` THEN
   INTRO_TAC "!n q; n q" THEN
   SUBGOAL_THEN `k INTER (0..n) = k`
     (fun th -> ASM_REWRITE_TAC[th; DIST_REFL]) THEN
   REWRITE_TAC[EXTENSION; IN_INTER; IN_NUMSEG; LE_0] THEN
   GEN_TAC THEN ASM_CASES_TAC `x:num IN k` THEN ASM_REWRITE_TAC[] THEN
   TRANS_TAC LE_TRANS `N:num` THEN ASM_SIMP_TAC[];
   POP_ASSUM (LABEL_TAC "fin")] THEN
  MP_TAC (ISPECL [`s:quat->bool`;`b:real`]
                 COMPACT_SHRINK_ENCLOSING_BALL_INFTY) THEN
  ANTS_TAC THENL [ASM_REWRITE_TAC[]; ALL_TAC] THEN
  INTRO_TAC "@r. r0 r1 r2" THEN
  MP_TAC (ISPECL[`\q:quat n. q pow n * a n`;
                 `\n. norm(a n:quat) * r pow n`;
                 `\q:quat. q IN s`; `k:num->bool`]
                SERIES_COMPARISON_UNIFORM) THEN
  ANTS_TAC THENL [ALL_TAC; MESON_TAC[]] THEN
  CONJ_TAC THENL
  [REWRITE_TAC[GSYM summable; GSYM REAL_SUMMABLE] THEN
   MATCH_MP_TAC REAL_CAUCHY_HADAMARD_RADIUS THEN
   EXISTS_TAC `b:real` THEN ASM_REWRITE_TAC[REAL_ABS_NORM] THEN
   ASM_SIMP_TAC[real_abs; REAL_LT_IMP_LE];
   ALL_TAC] THEN
  EXISTS_TAC `0` THEN REWRITE_TAC[LE_0] THEN INTRO_TAC "!n [z]; n z" THEN
  REWRITE_TAC[QUAT_NORM_MUL; QUAT_NORM_POW] THEN
  CONV_TAC (RATOR_CONV (ONCE_REWRITE_CONV[REAL_MUL_SYM])) THEN
  MATCH_MP_TAC REAL_LE_LMUL THEN
  ASM_SIMP_TAC[REAL_POW_LE2; NORM_POS_LE; REAL_LT_IMP_LE]);;

let QUAT_UNIFORM_CONV_POWER_SERIES_DERIVATIVE = prove
 (`!a b s k.
     ((\n. root n (norm (a n))) has_limsup b) (sequentially within k) /\
     compact s /\
     (!q. q IN s ==> b * norm q < &1)
     ==>
     ((\i q:quat. q pow (i - 1) * Hx(&i) * a i) uniformly_summable_on s) k`,
  REWRITE_TAC[uniformly_summable_on; uniformly_convergent_on;
              EVENTUALLY_SEQUENTIALLY] THEN
  REWRITE_TAC[MESON[]
    `(?l:quat->quat. !e. &0 < e ==> ?N:num. !n. N <= n ==> (!x:quat. x IN s
        ==> dist (vsum (k INTER (0..n))
                       (\i. x pow (i - 1) * Hx(&i) * a i),
                  l x) < e))
     <=>
     (?l:quat->quat. !e. &0 < e ==> ?N:num. !n q:quat. N <= n /\ q IN s
        ==> dist(vsum (k INTER (0..n))
                      (\i. q pow (i - 1) * Hx(&i) * a i),
                 l q) < e)`] THEN
  INTRO_TAC "!a s b k; limsup compact sub" THEN
  ASM_CASES_TAC `FINITE (k:num->bool)` THENL
  [POP_ASSUM MP_TAC THEN REWRITE_TAC[num_FINITE] THEN
   INTRO_TAC "@N. N" THEN
   EXISTS_TAC `\q. vsum k (\i. q pow (i - 1) * Hx(&i) * a i)` THEN
   INTRO_TAC "!e; epos" THEN EXISTS_TAC `N:num` THEN INTRO_TAC "!n q; n q" THEN
   SUBGOAL_THEN `k INTER (0..n) = k`
     (fun th -> ASM_REWRITE_TAC[th; DIST_REFL]) THEN
   REWRITE_TAC[EXTENSION; IN_INTER; IN_NUMSEG; LE_0] THEN
   GEN_TAC THEN ASM_CASES_TAC `x:num IN k` THEN ASM_REWRITE_TAC[] THEN
   TRANS_TAC LE_TRANS `N:num` THEN ASM_SIMP_TAC[];
   POP_ASSUM (LABEL_TAC "fin")] THEN
  CLAIM_TAC "@r. r1 r2 r3"
    `?r. &0 < r /\ b * r < &1 /\ (!q:quat. q IN s ==> norm q < r)` THENL
  [MATCH_MP_TAC COMPACT_SHRINK_ENCLOSING_BALL_INFTY THEN ASM_REWRITE_TAC[];
   ALL_TAC] THEN
  MP_TAC (ISPECL[`\q:quat n. q pow (n - 1) * Hx(&n) * a n`;
                 `\n. &n * norm(a n:quat) * r pow (n - 1)`;
                 `\q:quat. q IN s`; `k:num->bool`]
                SERIES_COMPARISON_UNIFORM) THEN
  ANTS_TAC THENL [ALL_TAC; MESON_TAC[]] THEN
  CONJ_TAC THENL
  [REWRITE_TAC[GSYM summable; GSYM REAL_SUMMABLE] THEN
   MATCH_MP_TAC REAL_CAUCHY_HADAMARD_RADIUS_DERIVATIVE THEN
   EXISTS_TAC `b:real` THEN ASM_REWRITE_TAC[REAL_ABS_NORM] THEN
   ASM_SIMP_TAC[real_abs; REAL_LT_IMP_LE];
   ALL_TAC] THEN
  EXISTS_TAC `0` THEN REWRITE_TAC[LE_0] THEN INTRO_TAC "!n [q]; n q" THEN
  REWRITE_TAC[QUAT_NORM_MUL; QUAT_NORM_POW; NORM_HX;
              REAL_ABS_NUM; REAL_MUL_ASSOC] THEN
  CONV_TAC (RATOR_CONV (REWRITE_CONV[
            REAL_ARITH`!x y z:real. (x * y) * z = (y * z) * x`])) THEN
  MATCH_MP_TAC REAL_LE_LMUL THEN
  ASM_SIMP_TAC[REAL_POW_LE2; NORM_POS_LE; REAL_LT_IMP_LE;
               REAL_LE_MUL; REAL_OF_NUM_LE; LE_0]);;

(* ------------------------------------------------------------------------- *)
(* Theorems about "vsum" on quaternions.                                     *)
(* ------------------------------------------------------------------------- *)

let VSUM_QUAT_LMUL = prove
 (`!c:quat f s. FINITE(s) ==> vsum s (\x:A. c * f x) = c * vsum s f`,
  GEN_TAC THEN GEN_TAC THEN MATCH_MP_TAC FINITE_INDUCT_STRONG THEN
  SIMP_TAC[VSUM_CLAUSES; QUAT_VEC_0; QUAT_MUL_RZERO] THEN
  SIMPLE_QUAT_ARITH_TAC);;

let VSUM_QUAT_RMUL = prove
 (`!c:quat f s. FINITE(s) ==> vsum s (\x:A. f x * c) = vsum s f * c`,
  GEN_TAC THEN GEN_TAC THEN MATCH_MP_TAC FINITE_INDUCT_STRONG THEN
  SIMP_TAC[VSUM_CLAUSES; QUAT_VEC_0; QUAT_MUL_RZERO] THEN
  SIMPLE_QUAT_ARITH_TAC);;

let VSUM_QUAT_POWER_SERIES_DERIVATIVE = prove
 (`!h a x n k.
      vsum (k INTER (0..n)) (\i. h * Hx (&i) * x pow (i - 1) * a i) =
      h * vsum (k INTER (0..n)) (\i. Hx (&i) * x pow (i - 1) * a i)`,
  REPEAT GEN_TAC THEN
  MATCH_MP_TAC VSUM_QUAT_LMUL THEN
  REWRITE_TAC[FINITE_INTER_NUMSEG]);;
